﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// The SPAWNER CLASS MUST CONTROL THE SPAWN OF NON-THROWABLE OBJECTS, 
/// WHICH MEANS OBJECTS THAT WILL ALWAYS BE SPAWNED WITHOUT MOVEMENT
/// FOR EXAMPLE: PLAYERS, ENEMIES AND USABLE ITENS
/// </summary>
public class Spawner : MonoBehaviour
{
    #region Variables

    /// <summary>
    /// The GameObject that will spawn when this Spawner is active.
    /// </summary>
    [SerializeField]
    private GameObject spawnable = null;
    public GameObject Spawnable { get => spawnable; set => spawnable = value; }

    /// <summary>
    /// Delay, in seconds, to throw a new spawnable after throwing the previous one.
    /// </summary>
    [SerializeField]
    private float cooldownTime;
    public float CooldownTime { get => cooldownTime; set => cooldownTime = value; }

    private float timer;

    [SerializeField]
    protected bool active = false;
    public bool Active { get => active; set => active = value; }

    #endregion Variables

    #region Methods

    public virtual GameObject Spawn(GameObject spawnable)
    {
        return Instantiate(spawnable, transform.position, Quaternion.identity, transform);
    }

    private void HandleSpawnTimer()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else if (timer <= 0)
        {
            timer = cooldownTime;

            Spawn(spawnable);
        }
    }

    #endregion Methods

    #region Callbacks

    protected virtual void Awake()
    {
        timer = cooldownTime;
    }

    private void Update()
    {
        if(active)
            HandleSpawnTimer();
    }

    protected virtual void OnDrawGizmos()
    {
        Gizmos.color = active ? Color.green : Color.red;
        Gizmos.DrawSphere(transform.position, 0.3f);
    }

    #endregion Callbacks

}
