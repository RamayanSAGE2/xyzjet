﻿using UnityEngine;

/// <summary>
/// THE THROWER CLASS MUST SHOOT OBJECTS IN MOVEMENT
/// FOR EXAMPLE: ASTEROIDS, BOMBS
/// </summary>
public class Thrower : Spawner
{

    #region Variables

    /// <summary>
    /// When calculating forces consider the Spawnable's mass on the Rigidibody2D component.
    /// </summary>
    [SerializeField]
    private bool useRigidbodyMass;

    /// <summary>
    /// Force that throws the Spawnable. 
    /// </summary>
    [SerializeField]
    [Range(0, 100)]
    private float force;
    public float Force { get => force; set => force = value; }

    [SerializeField]
    [Range(-180, 180)]
    private float angle;
    public float Angle { get => angle; set => angle = value; }

    /// <summary>
    /// Just stores the angle as a direction to use when throwing the Spawnable.
    /// </summary>
    private Vector2 direction;

    #endregion Variables

    #region Methods

    private void Throw(GameObject spawnable)
    {
        Rigidbody2D rb = spawnable.GetComponent<Rigidbody2D>();

        if (rb == null)
        {
            Debug.LogError(spawnable + " must have a Rigidbody2D component to be spawned with Thrower pattern.");
        }
        else
        {
            if (useRigidbodyMass)
            {
                rb.AddForce(direction * force, ForceMode2D.Impulse);
            }
            else
            {
                rb.velocity += direction * force;
            }
        }
    }

    public override GameObject Spawn(GameObject spawnable)
    {
        GameObject instance = Instantiate(spawnable, transform.position, Quaternion.identity, transform);

        Throw(instance);

        return instance;
    }

    #endregion Methods

    #region Callbacks

    protected override void Awake()
    {
        base.Awake();

        if (direction == Vector2.zero)
            direction = new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        if (direction == Vector2.zero)
            direction = new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position,
                        transform.position + (Vector3)direction);
    }

    #endregion Callbacks
}
