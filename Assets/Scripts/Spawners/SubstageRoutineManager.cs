﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[RequireComponent(typeof(SubStage))]
public class SubstageRoutineManager : MonoBehaviour
{
    [SerializeField]
    private bool debug = false;

    private int substage_id;
    private int stage_id;

    /// <summary>
    /// The throwers managed by an instance of this class.
    /// </summary>
    private Thrower[] throwers;

    /// <param name="thr_id"> The index on the throwers list</param>
    /// <param name="cooldown"> In seconds</param>
    /// <param name="angle"> In degrees</param>
    public void SetThrower(int thr_id, Projectile asteroid, float scale, float force, float angle)
    {
        SetThrower(thr_id, asteroid, scale);
        SetThrower(thr_id, force, angle);

    }

    public void SetThrowerForce(int thr_id, Projectile asteroid, float force)
    {
        SetThrower(thr_id, asteroid, 1);
        SetThrowerForce(thr_id, force);
    }

    /// <param name="thr_id"> The index on the throwers list</param>
    /// <param name="cooldown"> In seconds</param>
    /// <param name="angle"> In degrees</param>
    public void SetThrowerForce(int thr_id, float force)
    {
        if (thr_id >= 0 && thr_id < throwers.Length)
        {
            Thrower th = throwers[thr_id];
            th.Force = force;
        }
        else Debug.Log("INVALID THR ID");
    }

    /// <param name="thr_id"> The index on the throwers list</param>
    public void SetThrower(int thr_id, Projectile asteroid, float scale)
    {
        Thrower th = throwers[thr_id];
        GameObject gO = asteroid.gameObject;

        gO.transform.localScale = Vector2.one * scale;
        th.Spawnable = gO;
    }

    /// <param name="thr_id"> The index on the throwers list</param>
    /// <param name="cooldown"> In seconds</param>
    /// <param name="angle"> In degrees</param>
    public void SetThrower(int thr_id, float force, float angle)
    {
        Thrower th = throwers[thr_id];

        th.Force = force;
        th.Angle = angle;
    }

    /// <param name="thr_id"> The index on the throwers list</param>
    /// <param name="cooldown"> In seconds</param>
    /// <param name="angle"> In degrees</param>
    public void SetThrower(int thr_id, float cooldown, float force, float angle)
    {
        Thrower th = throwers[thr_id];

        th.Force = force;
        th.Angle = angle;
        th.CooldownTime = cooldown;
    }

    public void AddThrower(int thr_id, float force, float angle)
    {
        Thrower th = throwers[thr_id];

        th.Force += force;
        th.Angle += angle;
    }

    public void ActivateThrower(int thr_id)
    {
        throwers[thr_id].Active = true;
        //print("Activating " + throwers[thr_id].gameObject.name);
        //print(throwers[thr_id].Active);
    }

    public void DeactivateThrower(int thr_id)
    {
        throwers[thr_id].Active = false;
        //print("Deactivating " + throwers[thr_id].gameObject.name);
        //print(throwers[thr_id].Active);
    }

    private IEnumerator ActivateThrowerForSecondsCoroutine(int thr_id, float seconds, System.Action action)
    {
        ActivateThrower(thr_id);

        yield return new WaitForSeconds(seconds);

        DeactivateThrower(thr_id);

        action?.Invoke();
    }

    /// <param name="action"> If you want to call a function on thrower deactivation</param>
    public void ActivateThrowerForSeconds(int thr_id, float seconds, System.Action action)
    {
        StartCoroutine(ActivateThrowerForSecondsCoroutine(thr_id, seconds, action));
    }

    public void ActivateThrowerForSeconds(int thr_id, float seconds)
    {
        StartCoroutine(ActivateThrowerForSecondsCoroutine(thr_id, seconds, null));
    }

    public void ActivateAllThrowers()
    {
        for (int i = 0; i < throwers.Length; i++)
        {
            ActivateThrower(i);
        }
    }

    public void DeactivateAllThrowers()
    {
        for(int i = 0; i < throwers.Length; i++)
        {
            DeactivateThrower(i);
        }
    }

    /// <summary>
    /// Rotinas de substages para serem chamadas 1x na ativação das stages
    /// </summary>
    /// <returns></returns>
    #region Rotinas_de_Substages
    private IEnumerator TestSubstageRoutine()
    {
        SetThrower(1, DataManager.GetProjectile(0), 1);
        ActivateThrower(1);

        yield return new WaitForSeconds(2);

        SetThrower(1, DataManager.GetProjectile(1), 0.5f, 100, 45);
    }

    private IEnumerator Stage1Substage1Routine()
    {
        print("substage1");

        yield return new WaitForSeconds(1);
        SetThrowerForce(0, 5);
        SetThrowerForce(1, 5);
        //SCALE ALT
        SetThrower(0, DataManager.GetProjectile(0), 1);
        ActivateThrowerForSeconds(0,5);

        yield return new WaitForSeconds(5);
        SetThrower(1, DataManager.GetProjectile(1), 1);
        ActivateThrowerForSeconds(1, 5);

        yield return new WaitForSeconds(5);
        SetThrower(0, DataManager.GetProjectile(2), 2);
        ActivateThrowerForSeconds(0, 5);

        yield return new WaitForSeconds(5);
        SetThrower(1, DataManager.GetProjectile(3), 2);
        ActivateThrowerForSeconds(1, 5);

        yield return new WaitForSeconds(5);
        SetThrower(0, DataManager.GetProjectile(4), 3);
        ActivateThrowerForSeconds(0, 5);

        yield return new WaitForSeconds(5);
        SetThrower(1, DataManager.GetProjectile(1), 3);
        ActivateThrowerForSeconds(1, 5);

    }

    private IEnumerator Stage1Substage2Routine()
    {
        print("substage2");

        yield return new WaitForSeconds(1);
        //SPEED ALT
        SetThrowerForce(0, DataManager.GetProjectile(0), 10);
        ActivateThrowerForSeconds(0, 5);

        yield return new WaitForSeconds(5);
        SetThrowerForce(1, DataManager.GetProjectile(1), 15);
        ActivateThrowerForSeconds(1, 5);

        yield return new WaitForSeconds(5);
        SetThrowerForce(0, DataManager.GetProjectile(2), 20);
        ActivateThrowerForSeconds(0, 5);

        yield return new WaitForSeconds(5);
        SetThrowerForce(1, DataManager.GetProjectile(3), 25);
        ActivateThrowerForSeconds(1, 5);

        yield return new WaitForSeconds(5);
        SetThrowerForce(0, DataManager.GetProjectile(4), 35);
        ActivateThrowerForSeconds(0, 5);

        yield return new WaitForSeconds(5);
        SetThrowerForce(1, DataManager.GetProjectile(1), 40);
        ActivateThrowerForSeconds(1, 5);
    }

    private IEnumerator Stage1Substage3Routine()
    {
        print("substage3");

        SetThrowerForce(0, 5);
        SetThrowerForce(1, 5);
        SetThrower(0, DataManager.GetProjectile(0), 1);
        ActivateThrower(0);
        ActivateThrower(1);

        yield return new WaitForSeconds(3);

        DeactivateAllThrowers();

        SetThrowerForce(0, 15);
        SetThrowerForce(1, 15);
        ActivateThrowerForSeconds(0, 10);
        ActivateThrowerForSeconds(1, 10);
    }

    private IEnumerator Stage1Substage4Routine()
    {
        print("substage4");

        SetThrower(0, DataManager.GetProjectile(0), 1.5f,25,50);
        SetThrower(1, DataManager.GetProjectile(1), 1.5f, 25, 50);
        ActivateThrower(0);
        ActivateThrower(1);

        yield return new WaitForSeconds(1);

        DeactivateAllThrowers();

        SetThrowerForce(0, 15);
        SetThrowerForce(1, 15);
        ActivateThrowerForSeconds(0, 10);
        ActivateThrowerForSeconds(1, 10);
    }

    private IEnumerator StartSubstageRoutine()
    {
        //Wait substage transition
        yield return new WaitForSeconds(1);

        print(gameObject.name);

        if (debug)
        {
            StartCoroutine(TestSubstageRoutine());
        }
        else
        {

            if (stage_id == 1)
            {
                if (Stage.Current.ActiveSubstage.substage_id == 1)
                    StartCoroutine(Stage1Substage1Routine());
                else if (Stage.Current.ActiveSubstage.substage_id == 2)
                    StartCoroutine(Stage1Substage2Routine());
                else if (Stage.Current.ActiveSubstage.substage_id == 3)
                    StartCoroutine(Stage1Substage3Routine());
                else if (Stage.Current.ActiveSubstage.substage_id == 4)
                    StartCoroutine(Stage1Substage4Routine());
            }
            else if (stage_id == 2)
            {
                /*
                if (substage_id == 1) StartCoroutine(Stage2Substage1Routine());
                else if (substage_id == 2) StartCoroutine(Stage2Substage2Routine());
                else if (substage_id == 3) StartCoroutine(Stage2Substage3Routine());
                else if (substage_id == 4) StartCoroutine(Stage2Substage4Routine());
                */
            }
            else
                ActivateAllThrowers();
        }
    }

    #endregion Rotinas_de_Substages

    private void Awake()
    {
        return;

        RoundManager.OnEndRound += DeactivateAllThrowers;
        RoundManager.OnStartRound += () => { if (isActiveAndEnabled) StartCoroutine(StartSubstageRoutine()); };
        throwers = transform.GetComponentsInChildren<Thrower>();

        substage_id = this.gameObject.GetComponent<SubStage>().substage_id;
        stage_id = this.gameObject.GetComponentInParent<Stage>().stage_id;
    }

    private void OnDrawGizmos()
    {
        throwers = transform.GetComponentsInChildren<Thrower>();
        for (int i = 0; i < throwers.Length; i++)
            Handles.Label(throwers[i].transform.position, i.ToString());
    }
}




