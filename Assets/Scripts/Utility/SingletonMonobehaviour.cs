﻿using UnityEngine;

public class SingletonMonoBehaviour<T> : MonoBehaviour
  where T : MonoBehaviour
{
    public static T Instance { get; private set; }

    protected virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = this.GetComponent<T>();

            if(transform.root == transform)
                DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}