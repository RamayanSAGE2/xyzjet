﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Scoreboard : MonoBehaviour
{
    public Image[] ScoreScreenSprites;
    //[HideInInspector]
    //public TMP_Text[] ScoreScreenTexts;
    public Transform[] ScorePointParents;
    public GameObject ScorePointPrefab;

    private void Awake()
    {
       // ScoreScreenTexts = GetComponentsInChildren<TMP_Text>(true);
    }

    private void Start()
    {
        for(int i = 0; i < InGamePlayer.Players.Count; i++)
        {
            for(int j = 0; j < RoundManager.PointsToWin - 1; j++)
                Instantiate(ScorePointPrefab, ScorePointParents[i]);
        }
    }
}
