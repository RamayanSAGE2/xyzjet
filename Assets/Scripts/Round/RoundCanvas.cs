﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
public class RoundCanvas : MonoBehaviour
{
    [SerializeField]
    private TMP_Text countdown;
    [SerializeField]
    private Image panel;

    [SerializeField]
    private EndGameDisplay endGameDisplay;
    [SerializeField]
    private Scoreboard scoreboard;

    private int gameWinner;

    public void ShowScoreboard(int[] scores, Sprite[] scoreScreenSprites)
    {
        panel.gameObject.SetActive(true);

        for (int i = 0; i < scores.Length; i ++)
        {
            scoreboard.transform.GetChild(i).gameObject.SetActive(true);

            scoreboard.ScoreScreenSprites[i].sprite = scoreScreenSprites[i];

            for (int j = 0; j < scores[i]; j++)
                scoreboard.ScorePointParents[i].GetChild(j).GetComponent<Image>().color = Color.yellow;
        }
    }

    public void HideScoreboard()
    {
        panel.gameObject.SetActive(false);

        for (int i = 0; i < InGamePlayer.Players.Count; i++)
        {
            scoreboard.transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void ShowCountdown()
    {
        panel.gameObject.SetActive(true);

        countdown.gameObject.SetActive(true);
    }

    public void HideCountdown()
    {
        panel.gameObject.SetActive(false);

        countdown.gameObject.SetActive(false);
    }

    public void UpdateCountdown(int seconds)
    {
        countdown.text = seconds.ToString();
    }

    public void ShowEndGameDisplay(int[] scores, Sprite[] scoreScreenSprites)
    {
        int winner = 0;

        panel.gameObject.SetActive(true);

        for (int i = 0; i < InGamePlayer.Players.Count; i++)
        {
            endGameDisplay.transform.GetChild(i).gameObject.SetActive(true);

            endGameDisplay.characters[i].sprite = scoreScreenSprites[i];

            endGameDisplay.scores[i].text += scores[i];

            if (scores[i] > scores[winner])
                winner = i;
        }

        gameWinner = winner;
    }

    public IEnumerator UpdateEndGameDisplay()
    {
        RectTransform winnerRect = endGameDisplay.transform.GetChild(gameWinner).GetComponent<RectTransform>();

        Vector3 winnerPos = new Vector3(winnerRect.localPosition.x, endGameDisplay.WinnerPosY);

        while(winnerRect.anchoredPosition.y < endGameDisplay.WinnerPosY)
        {
            winnerRect.Translate(Vector3.up * endGameDisplay.WinnerSpeed);

            yield return new WaitForFixedUpdate();
        }
    }

    public void HideEndGameDisplay()
    {
        panel.gameObject.SetActive(false);

        for (int i = 0; i < InGamePlayer.Players.Count; i++)
        {
            endGameDisplay.transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
