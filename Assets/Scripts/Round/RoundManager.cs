﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoundManager : SingletonMonoBehaviour<RoundManager>
{
    [SerializeField]
    private RoundCanvas canvas;

    /// <summary>
    /// Number of points needed to win the game.
    /// </summary>
    [SerializeField]
    private int pointsToWin;
    public static int PointsToWin { get => Instance.pointsToWin; }
    //private int remainingRounds;

    [SerializeField]
    private int roundCountdownTime;

    [SerializeField]
    private bool debug;

    private float initialTimeScale = 0;

    /// <summary>
    /// On the current round.
    /// </summary>
    private int remainingCharacters;

    private bool roundActive;
    private bool firstRound = true;

    /// <summary>
    /// From each character
    /// </summary>
    private Sprite[] scoreScreenSprites = new Sprite[4];

    public static Action OnStartRound;
    public static Action OnEndRound;

    private IEnumerator StartRound()
    {
        Character[] characters = new Character[4];

        int i = 0;

        foreach (var p in InGamePlayer.Players)
        {
            characters[i] = Instantiate(p.SelectedPrefab, Stage.Current.ActiveSubstage.GetPlayerSpawnPoint(i), Quaternion.identity);

            p.Name = "P" + (i+1);

            p.Character = characters[i];

            if(firstRound)
            {
                Sprite s = characters[i].ScoreScreenSprite;
                scoreScreenSprites[i] = Sprite.Create(s.texture, s.textureRect, s.pivot);
            }

            i++;
        }

        if (firstRound)
            firstRound = false;

        remainingCharacters = i;

        Time.timeScale = 0;

        canvas.ShowCountdown();

        for(int timer = roundCountdownTime; timer >= 0; timer--)
        {
            canvas.UpdateCountdown(timer);

            yield return new WaitForSecondsRealtime(1);
        }

        canvas.HideCountdown();

        Time.timeScale = initialTimeScale;

        i = 0;

        foreach (var p in InGamePlayer.Players)
        {
            characters[i].Controller = p.Controller;

            i++;
        }

        roundActive = true;

        OnStartRound?.Invoke();
    }

    private IEnumerator EndRound()
    {
        Time.timeScale = 0;

        if (!debug)
        {
            InGamePlayer winner = null;

            for(int i = 0; i< InGamePlayer.Players.Count; i++)
            {
                InGamePlayer p = InGamePlayer.Players[i];

                if (p.Character != null)
                {
                    winner = p;
                }
            }

            if (winner != null)
            {
                winner.Score++;

                roundActive = false;

                canvas.ShowScoreboard(InGamePlayer.GetPlayersScore(), scoreScreenSprites);

                winner.Character.TryDestroy();

                yield return new WaitForSecondsRealtime(3);

                canvas.HideScoreboard();

                if (winner.Score != pointsToWin)
                {
                    if (Stage.Current != null)
                    {
                        Stage.Current.TransitionToNextSubstage(() =>
                        {
                            StartCoroutine(StartRound());
                        });
                    }
                    else
                    {
                        StartCoroutine(StartRound());
                    }
                }
                else
                {
                    StartCoroutine(EndGame());
                }
            }
        }
        else
        {
            StartCoroutine(StartRound());
        }

        Time.timeScale = initialTimeScale;

        OnEndRound?.Invoke();
    }

    private IEnumerator EndGame()
    {
        foreach (var e in OnStartRound.GetInvocationList())
            OnStartRound -= (Action)e;
        foreach (var e in OnEndRound.GetInvocationList())
            OnEndRound -= (Action)e;

        Time.timeScale = 0;

        canvas.ShowEndGameDisplay(InGamePlayer.GetPlayersScore(), scoreScreenSprites);

        Coroutine c = StartCoroutine(canvas.UpdateEndGameDisplay());

        yield return new WaitForSecondsRealtime(10);

        canvas.HideEndGameDisplay();

        Time.timeScale = initialTimeScale;

        firstRound = true;

        SceneManager.LoadScene("Menu");
    }

    protected override void Awake()
    {
        base.Awake();

        Character.DeathEvent += c => remainingCharacters--;

        initialTimeScale = Time.timeScale;
    }

    private void Start()
    {
        Stage.Current.TransitionToNextSubstage(() =>
        {
            StartCoroutine(StartRound());
        });
    }

    private void Update()
    {
        if (remainingCharacters <= (debug ? 0 : 1) && roundActive)
        {
            StartCoroutine(EndRound());
        }
    }
}