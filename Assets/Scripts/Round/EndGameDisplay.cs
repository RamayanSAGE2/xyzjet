﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameDisplay : MonoBehaviour
{
    public UnityEngine.UI.Image[] characters = new UnityEngine.UI.Image[4];
    public TMPro.TMP_Text[] scores = new TMPro.TMP_Text[4];

    public float WinnerPosY;
    public float WinnerSpeed;
}
