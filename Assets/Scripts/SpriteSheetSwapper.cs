﻿using UnityEngine;

public class SpriteSheetSwapper : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;

    private Sprite[] spriteSheet;

    [SerializeField]
    private string spriteSheetColorName;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void LateUpdate()
    {
    }

    private void LoadSpriteSheet()
    {
    }
}