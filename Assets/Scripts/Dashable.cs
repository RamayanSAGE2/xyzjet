﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(Collider2D))]
public class Dashable : MonoBehaviour
{
    private static List<Dashable> allDashables = new List<Dashable>();
    public static IReadOnlyList<Dashable> AllDashables => allDashables;

    public Collider2D Collider { get; private set; }

    private void Awake()
    {
        Collider = GetComponent<Collider2D>();
    }

    private void OnEnable()
    {
        allDashables.Add(this);
    }

    private void OnDisable()
    {
        allDashables.Remove(this);
    }
}