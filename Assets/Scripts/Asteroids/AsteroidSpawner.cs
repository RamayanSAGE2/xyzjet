﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class AsteroidSpawner : MonoBehaviour
{
    [Serializable]
    public class AsteroidSpawnBehaviour
    {
        // Data
        [HideInInspector]
        public AsteroidSpawner Spawner;
        
        public GameObject Prefab = null;
        public float InitialDelay = 10f;//OBS: TEMPO DE DELAY CONTANDO A PARTIR DO INICIO DA ATIVAÇÃO DA SUBSTAGE
                                        //OBS2: UM LANÇADOR SÓ PODE TER 1 COMPORTAMENTO POR VEZ, 
										// PORTANTO O DELAY DOS COMPORTAMENTOS SEGUINTES AO PRIMEIRO SÓ COMEÇAM A CONTAR APÓS QUE O ANTERIOR SE ACABE
										//OBS3: REMOVER BOOL DE ESTAR LIGADO INFINITAMENTE JÁ QUE SÓ FAZ SENTIDO SE ELE TIVER 1 COMPORAMENTO SÓ, CASO CONTRARIO 
										// PODERIA SE COMETER O ERRO DE ATRIBUIR 5 COMPORTAMENTOS E DEIXAR APENAS O PRIMEIRO LIGADO PARA SEMPRE
										//OBS4: MANTER OS INDICATIVOS DO EDITOR DE NUMERO E DIREÇÃO DE LANÇAMENTO QUE EXISTIAM NO LANÇADOR ANTERIOR, 
										//DESSA VEZ COM 2 INDICADORES DE DIREÇÃO, UM PARA ANGULO MAXIMO E OUTRO MINIMO, UM AMARELO E O OUTRO VERMELHO
        [Range(0, 360)]
        public float MinInitialDirectionAngle = 30f;
        [Range(0, 360)]
        public float MaxInitialDirectionAngle = 50f;
        
        public float MinInterval = 4f;
        public float MaxInterval = 4f;
        
        public float MinLaunchForce = 10f;
        public float MaxLaunchForce = 12f;
        
        public float MinBehaviourDuration = 0;
        public float MaxBehaviourDuration = 0;
        public bool LastsIndefinitely = true;

        // State
        [HideInInspector]
        public float AwakeTime;

        [HideInInspector]
        public float LastSpawnedAsteroidTime = 0;

        [HideInInspector]
        public float LastRolledSpawnInterval;

        [HideInInspector]
        public float RolledBehaviourDuration;

        public void Initialize(AsteroidSpawner spawner)
        {
            float now = Time.time;

            AwakeTime = now;

            LastRolledSpawnInterval = Random.Range(MinInterval, MaxInterval);
            LastSpawnedAsteroidTime = Mathf.Max(0, now - InitialDelay);
            RolledBehaviourDuration = Random.Range(MinBehaviourDuration, MaxBehaviourDuration);

            this.Spawner = spawner;
        }

        /// <summary>
        /// Performs this spawn data's actions and returns true if it has already ended its operations.
        /// </summary>
        public bool Tick()
        {
            if (Prefab == null)
            {
                return true;
            }

            float now = Time.time;

            if (now - LastSpawnedAsteroidTime >= LastRolledSpawnInterval)
            {
                float force = Random.Range(MinLaunchForce, MaxLaunchForce);
                float angle = Random.Range(MinInitialDirectionAngle, MaxInitialDirectionAngle);
                LastSpawnedAsteroidTime = now;
                LastRolledSpawnInterval = Random.Range(MinInterval, MaxInterval);

                var go = Instantiate(Prefab, Spawner.transform.position, Spawner.transform.rotation);

                if (go.TryGetComponent(out Rigidbody2D rb))
                {
                    float cos = Mathf.Cos(angle);
                    float sin = 1 - (cos * cos);
                    rb.AddForce(force * new Vector2(Mathf.Cos(angle), sin), ForceMode2D.Impulse);
                }
            }

            if (!LastsIndefinitely && now - AwakeTime >= RolledBehaviourDuration)
            {
                return true;
            }
            return false;
        }
    }
    [SerializeField]
    private List<AsteroidSpawnBehaviour> asteroidSpawnBehaviours;

    private void Start()
    {
        foreach (var a in asteroidSpawnBehaviours)
        {
            a.Initialize(this);
        }
    }
    
    private void Update()
    {
        for (int i = asteroidSpawnBehaviours.Count - 1; i >= 0; --i)
        {
            if (asteroidSpawnBehaviours[i].Tick())
            {
                asteroidSpawnBehaviours.RemoveAt(i);
            }
        }
    }
}