using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class Asteroid : Destroyable
{
    #region Unity Components

    private Rigidbody2D rigidbody;
    private Grabbable grabbable;

    #endregion Unity Components

    #region Variables

    [SerializeField]
    [Range(1f, 10)]
    private float scale = 1;
    public float Scale { get => scale; set => scale = value; }

    /// <summary>
    /// Use this if the asteroid is a plataform.
    /// </summary>
    [SerializeField]
    private bool isStationary;

    /// <summary>
    /// When calculating forces consider the Asteroid's mass on the Rigidibody2D component.
    /// </summary>
    [SerializeField]
    private bool useRigidbodyMass;

    /// <summary>
    /// If the asteroid push what it collided and is pushed to the opposite direction.
    /// </summary>
    [SerializeField]
    private bool ricochet = false;

    [SerializeField]
    [Range(0, 100)]
    private float ricochetForce;

    #endregion Variables

    #region Methods

    private Grabbable grabbableComponent;

    private bool IsBeingGrabbed()
    {
        if (grabbableComponent == null)
        {
            return false;
        }
        return grabbableComponent.Grabber != null;
    }

    private void ReScale()
    {
        transform.localScale = new Vector2(scale, scale);
    }

    #endregion Methods

    #region Callbacks

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (IsBeingGrabbed())
        {
            return;
        }

        if (collision.rigidbody != null && !isStationary && collision.collider.CompareTag("Player"))
        {
            //If their going on same direction and the rigidbody is behind the asteroid, the collision must not happen.
            float angle = Vector2.Angle((collision.rigidbody.position - rigidbody.position), rigidbody.velocity);

            //print(angle);
            if (angle < 90 || (angle > 270 && angle < 360))
            {
                Vector2 velocity;
                if (ricochet)
                {
                    velocity = new Vector2(rigidbody.velocity.normalized.x * ricochetForce * Mathf.Cos(angle), rigidbody.velocity.normalized.y / 2 * Mathf.Sin(angle));

                    if (useRigidbodyMass)
                    {
                        collision.rigidbody.AddForce(velocity, ForceMode2D.Impulse);
                        rigidbody.AddForce(-velocity, ForceMode2D.Impulse);
                    }
                    else
                    {
                        collision.rigidbody.velocity += velocity;
                        rigidbody.velocity -= velocity;
                    }
                }
                else
                {
                    velocity = new Vector2(rigidbody.velocity.x / 2 * Mathf.Cos(angle), rigidbody.velocity.y / 2 * Mathf.Sin(angle));

                    if (useRigidbodyMass)
                    {

                        collision.rigidbody.AddForce(velocity, ForceMode2D.Impulse);
                        rigidbody.AddForce(-velocity * 3 / 2 * Mathf.Cos(angle), ForceMode2D.Impulse);
                    }
                    else
                    {
                        collision.rigidbody.velocity += velocity;
                        rigidbody.velocity -= velocity;
                    }
                }
            }
        }

        if (collision.rigidbody != null && !isStationary && collision.collider.CompareTag("Platform"))
            SoundController.PlaySoundEffect(SOUNDS_ASTEROID.impact);
    }

    private void Awake()
    {
        if (rigidbody == null)
            rigidbody = this.GetComponent<Rigidbody2D>();

        if (isStationary)
            gameObject.layer = 8;

        grabbable = GetComponent<Grabbable>();

        RoundManager.OnEndRound += TryDestroy;
    }

    private void Start()
    {
        if (isStationary)
            rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    private void OnValidate()
    {
        ReScale();
    }

    #endregion Callbacks
}