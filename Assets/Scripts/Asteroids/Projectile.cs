﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class Projectile : Destroyable
{
    #region Unity Components

    private Rigidbody2D rigidbody;

    #endregion Unity Components

    #region Variables

    [SerializeField]
    [Range(1f, 10)]
    private float scale = 1;
    public float Scale { get => scale; set => scale = value; }

    /// <summary>
    /// When calculating forces consider the Asteroid's mass on the Rigidibody2D component.
    /// </summary>
    [SerializeField]
    private bool useRigidbodyMass;

    /// <summary>
    /// If the asteroid push what it collided and is pushed to the opposite direction.
    /// </summary>
    [SerializeField]
    private bool ricochet = false;

    [SerializeField]
    [Range(0, 10)]
    private float ricochetForce;

    #endregion Variables

    #region Methods

    private void ReScale()
    {
        transform.localScale = new Vector2(scale, scale);
    }

    private Grabbable grabbableComponent;

    private bool IsBeingGrabbed()
    {
        if (grabbableComponent == null)
        {
            return false;
        }
        return grabbableComponent.Grabber != null;
    }

    #endregion Methods

    #region Callbacks

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (IsBeingGrabbed())
        {
            return;
        }

        if (collision.rigidbody != null && collision.collider.CompareTag("Player"))
        {
            Vector2 dir = (collision.rigidbody.position - rigidbody.position).normalized;
            float angle = Vector2.Angle((collision.rigidbody.position - rigidbody.position), rigidbody.velocity);

            float cos = Mathf.Cos(angle);
            float sin = Mathf.Sin(angle);

            //If their going on same direction and the rigidbody is behind the asteroid, the collision must not happen.
            if (cos >= 0 && sin >= 0)
            {

                //Rotate the velocity to the direction it collided
                Vector2 velocity = new Vector2(rigidbody.velocity.x * cos + rigidbody.velocity.y * sin,
                                                -rigidbody.velocity.x * sin + rigidbody.velocity.y * cos);

                rigidbody.velocity = Vector2.zero;
                collision.rigidbody.velocity = Vector2.zero;

                if (ricochet)
                {

                    velocity.x *= ricochetForce;
                    velocity.y *= ricochetForce;

                    if (useRigidbodyMass)
                    {
                        collision.rigidbody.AddForce(velocity, ForceMode2D.Impulse);
                        rigidbody.AddForce(-velocity, ForceMode2D.Impulse);
                    }
                    else
                    {
                        collision.rigidbody.velocity += velocity;
                        rigidbody.velocity -= velocity;
                    }
                }
                else
                {

                    if (useRigidbodyMass)
                    {

                        collision.rigidbody.AddForce(velocity, ForceMode2D.Impulse);
                        rigidbody.AddForce(-velocity * 3 / 2 * Mathf.Cos(angle), ForceMode2D.Impulse);
                    }
                    else
                    {
                        collision.rigidbody.velocity += velocity;
                        rigidbody.velocity -= velocity;
                    }
                }
            }
        }

        if (collision.rigidbody != null && collision.collider.CompareTag("Platform"))
        {
            SoundController.PlaySoundEffect(SOUNDS_ASTEROID.impact);
        }
    }

    private void Awake()
    {
        rigidbody = this.GetComponent<Rigidbody2D>();

        rigidbody.freezeRotation = true;

        RoundManager.OnEndRound += TryDestroy;
        grabbableComponent = GetComponent<Grabbable>();
    }

    private void OnValidate()
    {
        ReScale();
    }

    #endregion Callbacks
}
