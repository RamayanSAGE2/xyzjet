﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Platform : MonoBehaviour
{
    [SerializeField]
    [Range(1f, 10)]
    private float scale = 1;
    public float Scale { get => scale; set => scale = value; }

    private void ReScale()
    {
        transform.localScale = new Vector2(scale, scale);
    }

    private void OnValidate()
    {
        ReScale();
    }

    private void Start()
    {
        gameObject.layer = 8;
    }
}
