﻿using UnityEngine;

public class FuelBar : MonoBehaviour
{
    private const float TRANSITION_DURATION = 1f;

    [SerializeField]
    private Character character;

    [SerializeField]
    private SpriteMask mask;

    [SerializeField]
    private SpriteRenderer fuelBarFilling;

    [SerializeField]
    private GameObject fuelBarRect;

    [SerializeField]
    private Color fullColor = Color.cyan;

    [SerializeField]
    private Color emptyColor = new Color(18f / 255f, 26f / 255f, 9f / 255f);
    
    private Vector3 originalMaskPosition;

    private float transitionTimer = 0f;

    private void Start()
    {
        originalMaskPosition = mask.transform.localPosition;
    }

    private void Update()
    {
        float ratio = character.CurrentFuel / 100;
        mask.transform.localPosition = Vector3.Lerp(fuelBarFilling.transform.localPosition, originalMaskPosition, ratio);
        fuelBarFilling.color = Color.Lerp(emptyColor, fullColor, ratio);

        if (fuelBarFilling.gameObject.activeSelf)
        {
            if (character.CurrentFuel == 100f)
            {
                transitionTimer -= Time.deltaTime;
                if (transitionTimer <= 0f)
                {
                    transitionTimer = 0f;
                    fuelBarFilling.gameObject.SetActive(false);
                    fuelBarRect.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            if (character.CurrentFuel != 100f)
            {
                transitionTimer = TRANSITION_DURATION;
                fuelBarFilling.gameObject.SetActive(true);
                fuelBarRect.gameObject.SetActive(true);
            }
        }
    }
}