﻿using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : Menu
{
    public void Button_Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}