﻿using UnityEngine;

public abstract class Menu : MonoBehaviour
{
    protected MenuManager MenuManager { get; private set; }

    [SerializeField]
    protected GameObject defaultFocusedGameObject = null;
    public GameObject DefaultFocusedGameObject => defaultFocusedGameObject;

    public virtual void OnEnter()
    {
    }

    public virtual void OnExit()
    {
    }

    protected virtual void Awake()
    {
        MenuManager = FindObjectOfType<MenuManager>();
    }
}