﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using TMPro;

public class StageSelectionMenu : Menu
{
    [Serializable]
    public struct Stage
    {
        public string Name;
        public string StageSceneName;
        public bool SelectRandomScene;
        public Sprite StagePreviewImage;
    }
    [SerializeField]
    private Stage[] stages;

    private int currentSelectedStageIndex = 0;

    [SerializeField]
    private Image previewImage;

    [SerializeField]
    private TextMeshProUGUI previewText;

    [SerializeField]
    private Button selectStageButton;

    private void UpdatePreview()
    {
        Stage s = stages[currentSelectedStageIndex];

        previewImage.sprite = s.StagePreviewImage;
        previewText.text = s.Name;
    }

    public override void OnEnter()
    {
        UpdatePreview();
        EventSystem.current.SetSelectedGameObject(selectStageButton.gameObject);
    }

    public void Button_SelectStage()
    {
        Stage selectedStage = stages[currentSelectedStageIndex];
        Stage finalStage;

        if (selectedStage.SelectRandomScene)
        {
            do
            {
                finalStage = stages[UnityEngine.Random.Range(0, stages.Length)];
            } while (finalStage.SelectRandomScene);
        }
        else
        {
            finalStage = selectedStage;
        }

        Debug.Log($"Selected stage {finalStage.Name}!");
        SceneManager.LoadScene(finalStage.StageSceneName);
    }

    private void ChangeCurrentSelectedStageIndex(bool positive)
    {
        if (positive)
        {
            currentSelectedStageIndex = (currentSelectedStageIndex + 1) % stages.Length;
        }
        else
        {
            currentSelectedStageIndex = (currentSelectedStageIndex + stages.Length - 1) % stages.Length;
        }
        UpdatePreview();
    }

    public void Button_PreviousStage()
    {
        ChangeCurrentSelectedStageIndex(false);
    }

    public void Button_NextStage()
    {
        ChangeCurrentSelectedStageIndex(true);
    }
}