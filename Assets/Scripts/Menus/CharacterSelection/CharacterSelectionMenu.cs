﻿using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections;
using System.Collections.Generic;

public class CharacterSelectionMenu : Menu
{
    /// <summary>
    /// Object instantiated by code that fires input events.
    /// </summary>
    private PlayerControls playerControls;
    
    /// <summary>
    /// Reference to the menu that should come right after this if all players confirm the game is ready.
    /// </summary>
    [SerializeField]
    private Menu nextMenu;

    /// <summary>
    /// Object responsible for handling input devices connection/disconnection.
    /// </summary>
    [SerializeField]
    private PlayerInputManager playerInputManager;

    /// <summary>
    /// Object that parents all slots. It is important that all character slots have this transform
    /// as their parent.
    /// </summary>
    [SerializeField]
    private Transform slotParentObject;

    /// <summary>
    /// True if all players have already confirmed they are ready to play.
    /// Required to prevent bugs from one frame to another when transitioning.
    /// </summary>
    private bool startingGame = false;

    /// <summary>
    /// True if new players are allowed to join.
    /// </summary>
    private bool joiningEnabled = false;

    private class PlayerStatus
    {
        public InGamePlayer Player;
        public CharacterSelectionSlot Slot;
        private bool ready;
        public bool Ready
        {
            get => ready;
            set
            {
                ready = value;
                if (Slot != null)
                {
                    Slot.Ready = ready;
                }
            }
        }
        public int SelectedCharacterIdx;

        /// <summary>
        /// The moment this player last changed their character.
        /// Required in order to implement the character change interval.
        /// </summary>
        public double LastCharacterChangeTime;
    }
    private Dictionary<int, PlayerStatus> playersStatus;

    /// <summary>
    /// Number of players that are already ready to play.
    /// </summary>
    private int readyPlayerCount = 0;

    private static List<PlayerInput> playerInputs = new List<PlayerInput>();

    private void JoinActionPerformed(InputAction.CallbackContext ctx)
    {
        int deviceId = ctx.control.device.deviceId;
        playerInputManager.JoinPlayerFromActionIfNotAlreadyJoined(ctx);
    }

    private bool IsCharacterIndexTaken(int i)
    {
        foreach (var p in playersStatus.Values)
        {
            if (p.SelectedCharacterIdx == i)
            {
                return true;
            }
        }
        return false;
    }

    private int ClaimFirstAvailableCharacterIndex()
    {
        for (int i = 0; i < DataManager.Instance.Playable_Characters.Length; ++i)
        {
            if (!IsCharacterIndexTaken(i))
            {
                return i;
            }
        }
        return DataManager.Instance.Playable_Characters.Length - 1;
    }

    protected override void Awake()
    {
        base.Awake();

        playerInputManager.onPlayerJoined += OnPlayerJoined;
        playerInputManager.onPlayerLeft += OnPlayerLeft;

        playerControls = new PlayerControls();
        playerControls.InGame.Disable();
    }
    
    private void OnConfirmButton(InputAction.CallbackContext ctx)
    {
        int deviceId = ctx.control.device.deviceId;

        if (playersStatus.TryGetValue(deviceId, out PlayerStatus status))
        {
            if (status.Ready == false)
            {
                status.Ready = true;
                readyPlayerCount++;
            }

            if (readyPlayerCount == InGamePlayer.Players.Count)
            {
                // All players are ready to start the game.
                startingGame = true;
                MenuManager.SetCurrentMenu(nextMenu);
            }
        }
    }

    private CharacterSelectionSlot ShowNewSlot(int slotId, int charId)
    {
        var go = slotParentObject.GetChild(slotId).gameObject;
        go.SetActive(true);
        var ret = go.GetComponent<CharacterSelectionSlot>();
        ret.SetSelectedCharacter(DataManager.Instance.Playable_Characters[charId]);
        return ret;
    }
    
    private void OnPlayerJoined(PlayerInput p)
    {
        if (!joiningEnabled)
        {
            Destroy(p.gameObject);
            return;
        }

        StartCoroutine(PlayerJoinedCoroutine(p));
        playerInputs.Add(p);
    }

    private IEnumerator PlayerJoinedCoroutine(PlayerInput p)
    {
        yield return new WaitForEndOfFrame();

        if (MenuManager.CurrentMenu != this)
        {
            yield break;
        }

        var controller = p.GetComponent<CharController>();

        int selectedIdx = ClaimFirstAvailableCharacterIndex();

        InGamePlayer player = new InGamePlayer
        {
            Controller = controller,
            SelectedPrefab = DataManager.Instance.Playable_Characters[selectedIdx]
        };

        DontDestroyOnLoad(controller.gameObject);

        PlayerStatus playerStatus = new PlayerStatus
        {
            Player = player,
            Ready = false,
            SelectedCharacterIdx = selectedIdx,
            Slot = ShowNewSlot(InGamePlayer.Players.Count, selectedIdx)
        };

        playersStatus[p.devices[0].deviceId] = playerStatus;

        InGamePlayer.RegisterPlayer(player);
    }

    private void OnPlayerLeft(PlayerInput p)
    {
    }

    public override void OnEnter()
    {
        playerControls.Enable();
        playerControls.Menu.Enable();
        playerControls.Menu.Confirm.Enable();

        playerControls.Menu.Confirm.performed += OnConfirmButton;
        playerControls.Menu.Join.performed += JoinActionPerformed;
        playerControls.Menu.ChangeCharacter.performed += OnAxisMove;

        playersStatus = new Dictionary<int, PlayerStatus>();

        for (int i = 0; i < slotParentObject.childCount; ++i)
        {
            slotParentObject.GetChild(i).gameObject.SetActive(false);
        }

        startingGame = false;
        readyPlayerCount = 0;

        joiningEnabled = true;

        InGamePlayer.UnregisterAllPlayers();

        foreach (var p in playerInputs)
        {
            Destroy(p.gameObject);
        }
        playerInputs.Clear();
    }

    public override void OnExit()
    {
        joiningEnabled = false;
        playerControls.Disable();
        playerControls.Menu.Disable();
        playerControls.Menu.Confirm.Disable();

        playerControls.Menu.Confirm.performed -= OnConfirmButton;
        playerControls.Menu.Join.performed -= JoinActionPerformed;
        playerControls.Menu.ChangeCharacter.performed -= OnAxisMove;

        playersStatus = null;
    }

    private void OnAxisMove(InputAction.CallbackContext ctx)
    {
        const double INTERVAL = 0.05;
        const float THRESHOLD = 0.8f;
        
       // print(ctx.duration);
        
        if (playersStatus.TryGetValue(ctx.control.device.deviceId, out PlayerStatus status))
        {
            if (ctx.time - status.LastCharacterChangeTime <= INTERVAL)
            {
                return;
            }
            status.LastCharacterChangeTime = ctx.time;

            // Read input
            Vector2 axes = ctx.ReadValue<Vector2>();
            
            if (axes.x > -THRESHOLD && axes.x < THRESHOLD)
            {
                return;
            }

            // Get direction
            int dir = (int)Mathf.Sign(axes.x);

            // Change character
            if (status != null)
            {
                int selectedIdx = status.SelectedCharacterIdx;
                do
                {
                    selectedIdx = (selectedIdx + DataManager.Instance.Playable_Characters.Length + dir)
                        % DataManager.Instance.Playable_Characters.Length;
                } while (IsCharacterIndexTaken(selectedIdx));
                status.SelectedCharacterIdx = selectedIdx;

                status.Slot.SetSelectedCharacter(DataManager.Instance.Playable_Characters[status.SelectedCharacterIdx]);

                status.Player.SelectedPrefab = DataManager.Instance.Playable_Characters[status.SelectedCharacterIdx];
            }
        }
    }
}