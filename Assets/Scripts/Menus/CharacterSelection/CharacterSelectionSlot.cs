﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class CharacterSelectionSlot : MonoBehaviour
{
    [SerializeField]
    private Image image;

    [SerializeField]
    private TextMeshProUGUI characterText;

    [SerializeField]
    private Image border;

    private bool ready;
    public bool Ready
    {
        get => ready;
        set
        {
            ready = value;

            if (border != null)
            {
                border.color = (value) ? Color.green : Color.white;
            }
        }
    }

    public void SetSelectedCharacter(Character c)
    {
        var spriteRenderer = c.GetComponent<SpriteRenderer>();
        
        if (spriteRenderer != null)
        {
            image.sprite = spriteRenderer.sprite;
            characterText.text = c.name;
        }        
    }

    private void Awake()
    {
        SetSelectedCharacter(DataManager.Instance.Playable_Characters[0]);
    }

    private void OnEnable()
    {
        Ready = false;
    }

    private void OnDisable()
    {
        Ready = false;
    }
}