﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class MenuManager : MonoBehaviour
{
    private Menu currentMenu = null;
    public Menu CurrentMenu => currentMenu;

    public void SetCurrentMenu(Menu menu)
    {
        // To prevent input conflicts, we need to wait a frame between disabling the current menu and
        // enabling the new one. For that, we use a coroutine that allows this conflict prevention.
        StartCoroutine(SetCurrentMenuCoroutine(menu));
    }

    private IEnumerator SetCurrentMenuCoroutine(Menu menu)
    {
        if (menu == null)
        {
            ClearVisitedMenusStack();
        }
        if (currentMenu != null)
        {
            visitedMenusStack.Push(currentMenu);
        }

        if (currentMenu != null)
        {
            currentMenu.OnExit();
            currentMenu.gameObject.SetActive(false);
        }

        yield return new WaitForEndOfFrame();

        if (menu != null)
        {
            menu.gameObject.SetActive(true);
            menu.OnEnter();
        }

        currentMenu = menu;

        if (menu.DefaultFocusedGameObject != null)
        {
            EventSystem.current.SetSelectedGameObject(menu.DefaultFocusedGameObject);
        }
    }

    private PlayerControls playerControls;

    /// <summary>
    /// Stack that contains the history of previously accessed menus.
    /// Nulls should not be pushed onto this stack.
    /// </summary>
    private Stack<Menu> visitedMenusStack = new Stack<Menu>();

    /// <summary>
    /// Returns to the previous menu. Does nothing if the previous menus' stack is empty.
    /// </summary>
    public void ReturnToPreviousMenu()
    {
        if (visitedMenusStack.Count == 0)
        {
            return;
        }

        Menu m = visitedMenusStack.Pop();

        SetCurrentMenu(m);

        if (visitedMenusStack.Count > 0)
        {
            visitedMenusStack.Pop();
        }
    }

    /// <summary>
    /// Clears the visited menus stack, zeroing current history of previously visited
    /// menus.
    /// </summary>
    public void ClearVisitedMenusStack()
    {
        visitedMenusStack.Clear();
    }

    private void Awake()
    {
        var menus = FindObjectsOfType<Menu>();
        foreach (var m in menus)
        {
            if (m.gameObject.activeSelf)
            {
                SetCurrentMenu(m);
                break;
            }
        }
    }

    private void OnBackRequested(InputAction.CallbackContext ctx)
    {
        ReturnToPreviousMenu();
    }

    private void Start()
    {
        playerControls = new PlayerControls();

        playerControls.Enable();
        playerControls.Menu.Enable();

        playerControls.Menu.Back.performed += OnBackRequested;
    }

    private void OnDestroy()
    {
        playerControls.Menu.Back.performed -= OnBackRequested;
        playerControls.Menu.Back.Disable();
        playerControls.Menu.Disable();
        playerControls.Disable();
        playerControls.Dispose();
    }
}