﻿using UnityEngine;

/// <summary>
/// A class that destroys all destroyables which exits its trigger collider.
/// </summary>
[RequireComponent(typeof(Collider2D))]
public class InstantDestructionArea : MonoBehaviour
{

    #region Variables

    #endregion Variables

    #region Methods

    #endregion Methods

    #region Callbacks

    private void OnTriggerExit2D(Collider2D collider)
    {
        var d = collider.GetComponent<Destroyable>();
        if (d != null)
        {
            d.TryDestroy();
        }
    }

    private void Awake()
    {
        if (GetComponent<Collider2D>().isTrigger == false)
            Debug.LogError("The Collider2D attached to " + name + "must be trigger.");
    }

    #endregion Callbacks
}