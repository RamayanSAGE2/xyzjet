﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class Destroyable : MonoBehaviour
{
    #region Variables

    /// <summary>
    /// Countdown to destroy this GameObject.
    /// </summary>
    private float timer;
    private bool timerActive;

    /// <summary>
    /// Prevent Destroy function to be called more than one time.
    /// </summary>
    private bool isBeingDestroyed = false;

    #endregion Variables

    #region Methods

    /// <summary>
    /// Start a countdown to destroy this GameObject.
    /// </summary>
    /// <param name="time"></param>
    public void StartTimer(float time)
    {
        timer = time;
        timerActive = true;
    }

    public void StopTimer()
    {
        timerActive = false;
    }

    public bool Invulnerable { get; set; }

    /// <summary>
    /// Calls the Destroy function, preventing it from being called multiple times.
    /// </summary>
    public void TryDestroy()
    {
        if (!isBeingDestroyed)
        {
            isBeingDestroyed = true;

            Destroy();
        }
    }

    protected virtual void Destroy()
    {
        if (Invulnerable)
        {
            return;
        }

        Destroy(this.gameObject);
    }

    #endregion Methods

    #region Callbacks

    protected virtual void Update()
    {
        if (timerActive)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
                TryDestroy();
        }
    }

    #endregion Callbacks
}