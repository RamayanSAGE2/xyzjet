﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class AliveArea : MonoBehaviour
{
    private class AliveClass
    {
        public GameObject gO;
        public float aT;
    }

    #region Variables

    /// <summary>
    /// The time the character lives after leaving the alive area.
    /// </summary>
    [SerializeField]
    private float aliveTimerDuration;

    #endregion Variables

    #region Methods

    #endregion Methods

    #region Callbacks

    private void OnTriggerExit2D(Collider2D collider)
    {
        var d = collider.GetComponent<Destroyable>();
        if(d != null && !d.Invulnerable)
        {
            d.StartTimer(aliveTimerDuration);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        var d = collider.GetComponent<Destroyable>();
        if (d != null)
        {
            d.StopTimer();
        }
    }

    private void Awake()
    {
        if (GetComponent<Collider2D>().isTrigger == false)
            Debug.LogError("The Collider2D attached to " + name + "must be trigger.");
    }

    #endregion Callbacks
}