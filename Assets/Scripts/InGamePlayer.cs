﻿using UnityEngine;
using System.Collections.Generic;

public class InGamePlayer
{
    public Character Character;
    public CharController Controller;

    public Character SelectedPrefab { get; set; }
    
    private static List<InGamePlayer> players = new List<InGamePlayer>();
    public static IReadOnlyList<InGamePlayer> Players => players;
    
    public string Name;

    public int Score;

    /// <summary>
    /// Registers a new player.
    /// </summary>
    public static void RegisterPlayer(InGamePlayer player)
    {
        players.Add(player);
    }

    /// <summary>
    /// Unregisters the specified player.
    /// </summary>
    public static void UnregisterPlayer(InGamePlayer player)
    {
        players.Remove(player);
    }

    public static void UnregisterAllPlayers()
    {
        foreach (var p in players)
        {
            GameObject.Destroy(p.Controller.gameObject);
        }
        players.Clear();
    }

    public static int[] GetPlayersScore()
    {
        int[] scores = new int[players.Count];

        for(int i = 0; i < scores.Length; i++)
        {
            InGamePlayer p = players[i];

            scores[i] = p.Score;
        }

        return scores;
    }
}