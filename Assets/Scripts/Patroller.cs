﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patroller : MonoBehaviour
{
    [Header("Patrol")]
    [SerializeField]
    private Transform[] points;
    [SerializeField]
    private float speed;

    private bool goingRight = true;

    private int currentPoint = 0;

    private void Patrol()
    {
        float x = 0;
        float y = 0;
        float step = speed * Time.deltaTime;
        Vector2 target = points[currentPoint].position;

        if (Vector2.Distance(transform.position, target) <= 0.5)
        {
            if (goingRight)
                currentPoint += 1;
            else
                currentPoint -= 1;

            if (currentPoint >= points.Length)
            {
                currentPoint = points.Length - 1;

                goingRight = false;
            }
            else if (currentPoint < 0)
            {
                currentPoint = 0;

                goingRight = true;
            }
        }

        transform.position = Vector2.MoveTowards(transform.position, target, step);
    }

    private void Update()
    {
        Patrol();
    }
}
