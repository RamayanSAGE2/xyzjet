﻿using UnityEngine;

public abstract class CharController : MonoBehaviour
{
    /// <summary>
    /// Returns the axes of movement this controller is currently
    /// requesting for. Each component of this vector holds a value
    /// between 0 and 1 (inclusive).
    /// </summary>
    public abstract Vector2 GetAxes();

    public abstract bool IsRequestingBoost();
    public abstract bool IsRequestingDash();
    public abstract bool IsRequestingJump();
    public abstract bool IsRequestingKick();
    public abstract bool IsRequestingGrab();
}