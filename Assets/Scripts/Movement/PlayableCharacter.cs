﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Collider2D)), 
    RequireComponent(typeof(Rigidbody2D)),
    RequireComponent(typeof(DashEffector))]
public class PlayableCharacter : Character
{
    #region Dash

    [SerializeField]
    private float dashCooldown;
    public float DashCooldown
    {
        get => dashCooldown;
        set => dashCooldown = value;
    }
    public float DashCooldownTimer { get; private set; }

    [Header("Dash")]
    [SerializeField]
    private float dashForce = 10f;
    public float DashForce
    {
        get => dashForce;
        set => dashForce = value;
    }

    private DashEffector dashEffector;

    private bool dashing = false;
    
    private IEnumerator DashCollisionIgnoreCoroutine()
    {
        List<Collider2D> ignoredColliders = new List<Collider2D>();
        for (int i = 0; i < Dashable.AllDashables.Count; ++i)
        {
            var d = Dashable.AllDashables[i];
            if (!Physics2D.GetIgnoreCollision(this.hitboxCollider, d.Collider))
            {
                Physics2D.IgnoreCollision(this.hitboxCollider, d.Collider, true);
                Physics2D.IgnoreCollision(d.Collider, this.hitboxCollider, true);
                Physics2D.IgnoreCollision(this.groundCollider, d.Collider, true);
                Physics2D.IgnoreCollision(d.Collider, this.groundCollider, true);
                ignoredColliders.Add(d.Collider);
            }
        }

        dashing = true;

        yield return new WaitForSeconds(0.8f);

        dashing = false;

        for (int i = 0; i < ignoredColliders.Count; ++i)
        {
            if (ignoredColliders[i].gameObject != null)
            {
                Physics2D.IgnoreCollision(this.hitboxCollider, ignoredColliders[i], false);
                Physics2D.IgnoreCollision(ignoredColliders[i], this.hitboxCollider, false);
                Physics2D.IgnoreCollision(this.groundCollider, ignoredColliders[i], false);
                Physics2D.IgnoreCollision(ignoredColliders[i], this.groundCollider, false);
            }
        }
    }

    /// <summary>
    /// Performs a quick dash towards the specified direction.
    /// If the jetpack is not flying, it won't do anything.
    /// </summary>
    /// <param name="dir">The direction to dash towards.</param>
    /// <param name="ignoreCooldown">If true, bypasses the dash cooldown and does not trigger its cooldown.</param>
    public void Dash(Vector2 dir, bool ignoreCooldown = false)
    {
        if (!Boosting || (DashCooldownTimer > 0f && !ignoreCooldown))
            return;

        rigidbody2d.velocity += DashForce * (Vector2)dir;

        if (!ignoreCooldown)
        {
            DashCooldownTimer = DashCooldown;
        }

        animator?.SetTrigger("Dash");
        dashEffector.PlayEffect();

        SoundController.PlaySoundEffect(SOUNDS_PLAYER.dash);

        StartCoroutine(DashCollisionIgnoreCoroutine());
    }

    private void DashCooldownTick()
    {
        DashCooldownTimer = Mathf.Max(DashCooldownTimer - Time.deltaTime, 0);
    }

    #endregion

    #region Jump

    [Header("Jump")]
    [SerializeField]
    private float jumpForce = 6f;
    public float JumpForce
    {
        get => jumpForce;
        set => jumpForce = value;
    }

    private float jumpSoundCdTimer = 0f;

    /// <summary>
    /// The jetpack jumps. If the jetpack is not touching the ground,
    /// it won't do anything.
    /// </summary>
    public void Jump()
    {
        if (!Grounded)
            return;

        rigidbody2d.AddForce(JumpForce * Vector2.up, ForceMode2D.Impulse);

        if (jumpSoundCdTimer == 0f)
        {
            SoundController.PlaySoundEffect(SOUNDS_PLAYER.jump);
            jumpSoundCdTimer = 0.25f;
        }
        animator?.SetTrigger("Jump");
    }

    private void JumpUpdate()
    {
        jumpSoundCdTimer = Mathf.Max(0f, jumpSoundCdTimer - Time.deltaTime);
    }

    #endregion

    #region Punch

    private bool IsValidPunchOpponent(Rigidbody2D c) // To be implemented
    {
        return true;
    }

    [Header("Punch")]
    [SerializeField]
    private float punchCooldown = 2f;
    public float PunchCooldown
    {
        get => punchCooldown;
        set => punchCooldown = Mathf.Max(0f, value);
    }
    private float punchCooldownTimer = 0f;

    [SerializeField]
    private float punchForce = 12f;
    public float PunchForce
    {
        get => punchForce;
        set => punchForce = value;
    }

    [Header("Colliders")]
    [SerializeField]
    [Tooltip("Collider that applies kicks")]
    private Collider2D punchCollider;

    [SerializeField]
    [Tooltip("Collider for uppercut punches")]
    private Collider2D uppercutCollider;
        
    private void DrawDebugPunchRect()
    {
        const float DUR = 0.5f;

        Rect rect = new Rect(punchCollider.bounds.min, punchCollider.bounds.size);

        Debug.DrawLine(new Vector3(rect.xMin, rect.yMin, 2f), new Vector3(rect.xMax, rect.yMin, 2f), Color.red, DUR);
        Debug.DrawLine(new Vector3(rect.xMin, rect.yMin, 2f), new Vector3(rect.xMin, rect.yMax, 2f), Color.red, DUR);
        Debug.DrawLine(new Vector3(rect.xMin, rect.yMax, 2f), new Vector3(rect.xMax, rect.yMax, 2f), Color.red, DUR);
        Debug.DrawLine(new Vector3(rect.xMax, rect.yMin, 2f), new Vector3(rect.xMax, rect.yMax, 2f), Color.red, DUR);
    }
    
    /// <summary>
    /// Cached array for enemies hit by kick.
    /// </summary>
    private Collider2D[] punchResults = new Collider2D[6];

    public void Punch()
    {
        if (punchCollider == null)
        {
            Debug.LogWarning($"Kick collider not setup for {name}! Kick request has been ignored.");
            return;
        }

        if (punchCooldownTimer > 0f || dashing)
        {
            return;
        }

        punchCooldownTimer = PunchCooldown;

        DrawDebugPunchRect();

        ContactFilter2D filter = new ContactFilter2D();

        int len;

        Vector2 axes = Controller.GetAxes();

        if (Controller != null && axes.y > (Mathf.Abs(axes.x) + 0.4f))
        {
            animator.SetTrigger("Uppercut");
            len = uppercutCollider.OverlapCollider(filter, punchResults);
            for (int i = 0; i < len; ++i)
            {
                var otherRb = punchResults[i].attachedRigidbody;

                if (otherRb == null || otherRb == rigidbody2d || !IsValidPunchOpponent(otherRb))
                {
                    continue;
                }
                else
                {
                    SoundController.PlaySoundEffect(SOUNDS_PLAYER.kick);
                    otherRb.AddForce(PunchForce * (otherRb.position - new Vector2(punchCollider.bounds.center.x - punchCollider.bounds.extents.x * Mathf.Sign(Facing), punchCollider.bounds.center.y)).normalized, ForceMode2D.Impulse);
                    return;
                }
            }
        }
        else
        {
            len = punchCollider.OverlapCollider(filter, punchResults);
            animator.SetTrigger("Kick");
            for (int i = 0; i < len; ++i)
            {
                var otherRb = punchResults[i].attachedRigidbody;

                if (otherRb == null || otherRb == rigidbody2d || !IsValidPunchOpponent(otherRb))
                {
                    continue;
                }
                else
                {
                    SoundController.PlaySoundEffect(SOUNDS_PLAYER.kick);
                    otherRb.AddForce(PunchForce * (otherRb.position - new Vector2(punchCollider.bounds.center.x - punchCollider.bounds.extents.x * Mathf.Sign(Facing), punchCollider.bounds.center.y)).normalized, ForceMode2D.Impulse);
                    break;
                }
            }
        }
    }

    private void KickUpdate()
    {
        punchCooldownTimer = Mathf.Max(0f, punchCooldownTimer - Time.deltaTime);
    }

    #endregion

    /// <summary>
    /// Checks for input from the controller and handles its requests accordingly.
    /// </summary>
    private void DelegateControllerOrders()
    {
        if (Controller == null)
            return;

        if (Controller.IsRequestingJump())
        {
            Jump();
        }

        if (Controller.IsRequestingDash())
        {
            Dash(GetDirectionAxes());
        }

        if (Controller.IsRequestingKick())
        {
            Punch();
        }
    }

    #region Unity Callbacks

    protected override void Awake()
    {
        base.Awake();

        dashEffector = GetComponent<DashEffector>();
    }

    protected override void Update()
    {
        base.Update();

        DashCooldownTick();
        DelegateControllerOrders();
        KickUpdate();
        JumpUpdate();
    }

    #endregion
}