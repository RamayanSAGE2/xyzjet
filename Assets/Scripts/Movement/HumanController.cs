﻿using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// Controller that handles input from human sources. Automatically handles different
/// inputs from keyboards, joysticks and gamepads.
/// </summary>
[RequireComponent(typeof(PlayerInput))]
public class HumanController : CharController
{
    private Vector2 movementAxes;

    private bool requestingDash;
    private bool requestingJump;
    private bool requestingKick;
    private bool releasedGrab = true;
    private bool requestingGrab = false;

    private void OnMovement(InputValue value)
    {
        movementAxes = value.Get<Vector2>();
    }
    
    private void OnDash(InputValue value)
    {
        requestingDash = value.isPressed;
    }

    private void OnJump(InputValue value)
    {
        requestingJump = value.isPressed;
    }

    private void OnKick(InputValue value)
    {
        requestingKick = value.isPressed;
    }
    
    private void OnGrab(InputValue value)
    {
        requestingGrab = value.isPressed;
        print(value.isPressed);
    }

    private void OnPause(InputValue v)
    {
        Time.timeScale = (Time.timeScale == 0f) 
                                   ? 1f 
                                   : 0f;
    }

    public override Vector2 GetAxes()
    {
        return movementAxes;
    }

    public override bool IsRequestingBoost()
    {
        return movementAxes.x != 0 || movementAxes.y != 0;
    }

    public override bool IsRequestingDash()
    {
        return requestingDash;
    }

    public override bool IsRequestingJump()
    {
        return requestingJump;
    }

    public override bool IsRequestingKick()
    {
        return requestingKick;
    }

    public override bool IsRequestingGrab()
    {
        return requestingGrab;
    }
}