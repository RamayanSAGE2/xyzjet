﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Component that contains Joe's special dash effects.
/// </summary>
public class DashEffector : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;

    private Transform dashTrailParent;

    [SerializeField]
    private int trailImageCount = 5;

    public float ShadowFadeDuration = 1f;
    public float TrailFormationDuration = 1f;
    public float MaxShadowAlpha = 0.8f;

    private SpriteRenderer[] shadowPool;    
    
    private bool canPlayEffect = true;

    private IEnumerator EffectCoroutine()
    {
        // The effect should follow as described:
        // 
        // Every 'trailFormationDuration'/'trailImageCount' seconds, a shadow should be spawned at the position of the character.
        // Once spawned, the shadow will fade out in 'shadowFadeDuration' seconds.
        //
        // The alpha of the shadows is handled in the 'UpdateShadowsAlpha' method.
        canPlayEffect = false;
        WaitForSeconds shadowSpawnInterval = new WaitForSeconds(TrailFormationDuration / trailImageCount);

        for (int i = 0; i < trailImageCount; ++i)
        {
            // The spawned sprites position, rotation and scale must match those of
            // the character.
            var s = shadowPool[i % shadowPool.Length];
            var t = s.transform;

            s.gameObject.SetActive(true);
            s.sprite = spriteRenderer.sprite;
            s.color = new Color(s.color.r, s.color.g, s.color.b, MaxShadowAlpha);
            t.position = transform.position;
            t.localScale = transform.lossyScale;
            t.rotation = transform.rotation;
            yield return shadowSpawnInterval;
        }

        // Allow the effect to start once again.
        canPlayEffect = true;
    }

    private void UpdateShadowsAlpha()
    {
        for (int i = 0; i < shadowPool.Length; ++i)
        {
            var s = shadowPool[i];

            if (s.gameObject.activeSelf)
            {
                s.color = new Color(s.color.r, s.color.g, s.color.b,
                    s.color.a - (MaxShadowAlpha / ShadowFadeDuration) * Time.deltaTime);

                if (s.color.a <= 0f)
                {
                    s.gameObject.SetActive(false);
                }
            }
        }
    }

    /// <summary>
    /// Plays the dash effect.
    /// </summary>
    public void PlayEffect()
    {
        if (!canPlayEffect)
        {
            return;
        }

        StartCoroutine(EffectCoroutine());
    }

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        dashTrailParent = new GameObject().transform;
        dashTrailParent.gameObject.name = $"{this.name} Dash Trails";

        // Generate pool of shadow sprite renderers:
        shadowPool = new SpriteRenderer[trailImageCount];

        for (int i = 0; i < shadowPool.Length; ++i)
        {
            var go = new GameObject();
            go.name = "DashTrail";
            go.transform.SetParent(dashTrailParent);

            var sr = go.AddComponent<SpriteRenderer>();
            sr.sortingLayerID = spriteRenderer.sortingLayerID;
            sr.sortingOrder = spriteRenderer.sortingOrder - 1;
            sr.material = spriteRenderer.material; 

            go.SetActive(false);

            shadowPool[i] = sr;
        }
    }

    private void Update()
    {
        UpdateShadowsAlpha();
    }

    private bool destroyed = false;

    private void OnDestroy()
    {
        if (destroyed)
        {
            return;
        }
        destroyed = true;

        if (dashTrailParent != null)
        {
            Destroy(dashTrailParent.gameObject);
        }
    }

    private void OnDisable()
    {
        if (destroyed)
        {
            return;
        }

        foreach (var s in shadowPool)
        {
            if (s != null)
            {
                s.gameObject.SetActive(true);
            }
        }
    }
}