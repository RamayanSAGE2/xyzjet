﻿using UnityEngine;

public class KeyboardController : CharController
{
    [SerializeField]
    private string horizontalAxis = "Horizontal";
    [SerializeField]
    private string verticalAxis = "Vertical";

    [SerializeField]
    private KeyCode dashKey = KeyCode.LeftShift;
    [SerializeField]
    private KeyCode jumpKey = KeyCode.Space;
    [SerializeField]
    private KeyCode kickKey = KeyCode.E;

    public override Vector2 GetAxes()
    {
        return new Vector2(Input.GetAxis(horizontalAxis), Input.GetAxis(verticalAxis));
    }

    public override bool IsRequestingBoost()
    {
        return Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0;
    }

    public override bool IsRequestingDash()
    {
        return Input.GetKeyDown(dashKey);
    }

    public override bool IsRequestingGrab()
    {
        return false;
    }

    public override bool IsRequestingJump()
    {
        return Input.GetKeyDown(jumpKey);
    }

    public override bool IsRequestingKick()
    {
        return Input.GetKeyDown(kickKey);
    }
}