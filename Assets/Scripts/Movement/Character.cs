﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Base class for characters. Contains common movement mechanics that'll be shared among all
/// characters in the game.
/// </summary>
public abstract class Character : Destroyable
{
    [SerializeField]
    private Sprite scoreScreenSprite;
    public Sprite ScoreScreenSprite { get => scoreScreenSprite; }

    [SerializeField]
    private CharController controller;
    public CharController Controller
    {
        get => controller;
        set => controller = value;
    }

    [Header("Walk")]
    [SerializeField]
    private float walkSpeed = 3f;      

    [SerializeField]
    protected Collider2D hitboxCollider;

    [SerializeField]
    protected Collider2D groundCollider;

    #region Cached Values

    protected Rigidbody2D rigidbody2d;
    protected Animator animator;

    protected int groundLayerMask;

    #endregion

    #region Movement

    /// <summary>
    /// True if this jetpack is not touching the ground and not flying.
    /// </summary>
    public bool Falling => !Boosting && !Grounded;

    /// <summary>
    /// True if this jetpack is currently touching the ground.
    /// </summary>
    public bool Grounded { get; private set; }

    /// <summary>
    /// Number between -1 and 1 that determines what direction this jetpack is facing.
    /// If greater than or equal to 0, it's facing right;
    /// Otherwise, it is facing left.
    /// </summary>
    private float facing = 0f;
    public float Facing
    {
        get => facing;
        set
        {
            value = Mathf.Clamp(value, -1f, 1f);
            if (Mathf.Sign(value) != Mathf.Sign(facing))
            {
                Flip();
            }
            facing = value;
        }
    }

    private void UpdateGroundedStatus()
    {
        Grounded = groundCollider.IsTouchingLayers(groundLayerMask);

        animator.SetBool("Grounded", Grounded);

        if (Grounded && !Boosting)
        {
            rigidbody2d.SetRotation(Quaternion.identity);
            rigidbody2d.gravityScale = 5;
        }
        else
        {
            rigidbody2d.gravityScale = gravityScaleDelta;
        }
    }

    private void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
    
    public bool Boosting { get; private set; }

    [Header("Flight")]
    [SerializeField]
    private JetpackFireEffect fireEffect;
    public JetpackFireEffect FireEffect
    {
        get => fireEffect;
        set => fireEffect = value;
    }

    [SerializeField]
    private float boostAcceleration = 20f;
    public float BoostAcceleration
    {
        get => boostAcceleration;
        set => boostAcceleration = value;
    }

    private float currentBoostSpeed = 0f;

    [SerializeField]
    private float boostTopSpeed = 20f;
    public float BoostTopSpeed
    {
        get => boostTopSpeed;
        set => boostTopSpeed = value;
    }

    [SerializeField]
    [Range(0, 359)]
    private float maximumTiltAngle = 40f;
    public float MaximumTiltAngle
    {
        get => maximumTiltAngle;
        set => maximumTiltAngle = value % 360;
    }

    /// <summary>
    /// Used by 'StartFlying()' to return to a previous value of gravity scale once the jetpack
    /// finishes its flight.
    /// </summary>
    private float gravityScaleDelta = 0f;

    public void StartFlying()
    {
        if (Boosting || (CurrentFuel < FlightMinFuelRequired && !hasInfiniteFuel))
            return;

        Boosting = true;
        animator.SetBool("Flying", true);
        SoundController.PlaySoundEffect(SOUNDS_PLAYER.jetpack_turn_on);

        // The following value is set to be able to return to the gravity scale this jetpack was using before
        // it started flying.
        gravityScaleDelta = 0f - rigidbody2d.gravityScale;
        rigidbody2d.gravityScale = 0f;

        FireEffect?.Play();
    }

    public void StopFlying()
    {
        if (!Boosting)
            return;

        Boosting = false;
        animator.SetBool("Flying", false);
        SoundController.PlaySoundEffect(SOUNDS_PLAYER.jetpack_turn_off);

        // Return to the previous gravity scale.
        rigidbody2d.gravityScale -= gravityScaleDelta;

        FireEffect?.Stop();
    }

    private void FlightFixedUpdate(Vector2 axes)
    {
        if (Boosting)
        {
            if (axes.y >= 0)
            {
                fireEffect.transform.localScale = new Vector3(fireEffect.transform.localScale.x, Mathf.Abs(fireEffect.transform.localScale.y));
            }
            else
            {
                fireEffect.transform.localScale = new Vector3(fireEffect.transform.localScale.x, -Mathf.Abs(fireEffect.transform.localScale.y));
            }

            float max;

            // Apply vertical speed
            if ((rigidbody2d.velocity.y < BoostTopSpeed && axes.y > 0))
            {
                max = BoostTopSpeed - rigidbody2d.velocity.y;
                rigidbody2d.velocity += Mathf.Min(max, BoostAcceleration * Time.fixedDeltaTime) * Vector2.up * axes.y;
            }
            else if (rigidbody2d.velocity.y > -BoostTopSpeed && axes.y < 0)
            {
                max = -BoostTopSpeed + rigidbody2d.velocity.y;
                rigidbody2d.velocity += Mathf.Max(max, BoostAcceleration * Time.fixedDeltaTime) * Vector2.up * axes.y;
            }

            // Apply horizontal speed
            if ((rigidbody2d.velocity.x < BoostTopSpeed && axes.x > 0))
            {
                max = BoostTopSpeed - rigidbody2d.velocity.x;
                rigidbody2d.velocity += Mathf.Min(max, BoostAcceleration * Time.fixedDeltaTime) * Vector2.right * axes.x;
            }
            else if (rigidbody2d.velocity.x > -BoostTopSpeed && axes.x < 0)
            {
                max = -BoostTopSpeed + rigidbody2d.velocity.x;
                rigidbody2d.velocity += Mathf.Max(max, BoostAcceleration * Time.fixedDeltaTime) * Vector2.right * axes.x;
            }
        }

        if (!Grounded && Boosting)
        {
            // Apply tilt
            rigidbody2d.rotation = Mathf.LerpAngle(rigidbody2d.rotation, axes.x * -MaximumTiltAngle * Mathf.Sign(axes.y), 10 * Time.fixedDeltaTime);
        }
        else if (!Boosting)
        {
            // Apply original rotation again
            rigidbody2d.rotation = Mathf.LerpAngle(rigidbody2d.rotation, 0f, 10 * Time.fixedDeltaTime);
        }
    }

    #endregion

    #region Fuel

    [Header("Fuel")]
    public bool hasInfiniteFuel = false;

    [Range(0, 100)]
    [SerializeField]
    private float currentFuel = float.PositiveInfinity; // This gets corrected automatically by OnValidate.
    public float CurrentFuel
    {
        get => currentFuel;
        set => currentFuel = Mathf.Clamp(value, 0f, 100);
    }

    [SerializeField]
    private float fuelLossPerSecond = 8f;
    public float FuelLossPerSecond
    {
        get => fuelLossPerSecond;
        set => fuelLossPerSecond = value;
    }

    [SerializeField]
    private float fuelRecoveryPerSecond = 6f;
    public float FuelRecoveryPerSecond
    {
        get => fuelRecoveryPerSecond;
        set => fuelRecoveryPerSecond = value; // Maybe set minimum to zero later?
    }

    [SerializeField]
    [Tooltip("The minimum fuel the jetpack needs to start flying.")]
    private float flightMinFuelRequired = 10f;
    public float FlightMinFuelRequired
    {
        get => flightMinFuelRequired;
        set => flightMinFuelRequired = value;
    }

    /// <summary>
    /// Combustível enche gradativamente se o personagem estiver parado ou agarrado em algo
    /// Se o combustível esvaziar totalmente demora mais a encher
    /// Crirar variaveis para velocidade de enchimento e uso do combustivel
    /// </summary>
    private void FuelUpdate()
    {
        if (hasInfiniteFuel)
            return;

        if (Boosting)
        {
            currentFuel -= Time.deltaTime * fuelLossPerSecond;

            if (currentFuel <= 0f)
            {
                currentFuel = 0f;
                FireEffect?.Stop();
                Invoke(nameof(StopFlying), 1f); // No need to ask if already flying, StopFlying already handles this.
            }
        }
        else
        {
            CurrentFuel += Time.deltaTime * fuelRecoveryPerSecond;
        }
    }

    #endregion

    #region Grab

    [Header("Grab")]
    [SerializeField]
    private float grabMaxRange = 1f;

    [SerializeField]
    private Collider2D grabCollider;

    [SerializeField]
    private Transform grabHandPosition;

    [SerializeField]
    private Transform grabHandPositionGround;

    [SerializeField]
    private float throwForce = 10f;

    [SerializeField]
    private float backlashForce = 4f;
    
    private Collider2D[] grabResults = new Collider2D[6];
    
    /// <summary>
    /// Object being 
    /// </summary>
    private Grabbable currentlyGrabbedObject;

    private float grabCooldown = 0.2f;

    private float grabCooldownTimer = 0;
        
    /// <summary>
    /// Tries to grab the nearest object from the hero. 
    /// </summary>
    /// <returns>
    /// The grabbed object, or null if none is grabbed.
    /// </returns>
    public Grabbable TryGrabNearest()
    {
        if (currentlyGrabbedObject != null || grabCooldownTimer > 0f)
        {
            return null;
        }

        int count = Physics2D.OverlapCollider(grabCollider, new ContactFilter2D(), grabResults);
        animator.SetTrigger("Grab");

        grabCooldownTimer = grabCooldown;

        for (int i = 0; i < count; ++i)
        {
            var grabbable = grabResults[i].GetComponent<Grabbable>();

            if (grabbable != null)
            {
                animator.SetBool("Grabbing", true);

                currentlyGrabbedObject = grabbable;
                grabbable.OnGrab(gameObject);
                grabbable.transform.SetParent(grabHandPosition);
                grabbable.transform.localPosition = Vector3.zero;
                grabbable.Collider.isTrigger = true;
                grabbable.Collider.enabled = false;
                grabbable.Rigidbody.isKinematic = true;
                grabbable.Rigidbody.simulated = false;
                return grabbable;
            }
        }

        return null;
    }

    /// <summary>
    /// Throws the currently grabbed object towards the direction this character is currently facing.
    /// Does nothing if no object is currently being grabbed.
    /// </summary>
    public void ThrowGrabbedObject()
    {
        if (currentlyGrabbedObject == null || grabCooldownTimer > 0f)
        {
            return;
        }

        StartCoroutine(ThrowGrabbedCoroutine());
    }

    private IEnumerator ThrowGrabbedCoroutine()
    {
        animator.SetBool("Grabbing", false);
        animator.SetTrigger("Throw");

        grabCooldownTimer = grabCooldown;
        
        currentlyGrabbedObject.Rigidbody.isKinematic = false;
        currentlyGrabbedObject.transform.SetParent(null, true);
        currentlyGrabbedObject.OnThrow();
        currentlyGrabbedObject.Collider.enabled = true;
        currentlyGrabbedObject.Collider.isTrigger = false;
        currentlyGrabbedObject.Rigidbody.simulated = true;
        currentlyGrabbedObject.Rigidbody.AddForce(throwForce * Vector3.right * Mathf.Sign(transform.localScale.x), ForceMode2D.Impulse);
        rigidbody2d.AddForce(backlashForce * Vector3.left * Mathf.Sign(transform.localScale.x), ForceMode2D.Impulse);

        currentlyGrabbedObject = null;

        yield break;
    }

    private void GrabUpdate()
    {
        grabCooldownTimer = Mathf.Max(grabCooldownTimer - Time.deltaTime, 0f);

        if (currentlyGrabbedObject != null)
        {
            if (Grounded && currentlyGrabbedObject.transform.parent != grabHandPositionGround)
            {
                currentlyGrabbedObject.transform.parent = grabHandPositionGround;
                currentlyGrabbedObject.transform.localPosition = new Vector3(0, 0, 0);
            }
            else if (!Grounded && currentlyGrabbedObject.transform.parent != grabHandPosition)
            {
                currentlyGrabbedObject.transform.parent = grabHandPosition;
                currentlyGrabbedObject.transform.localPosition = new Vector3(0, 0, 0);
            }
        }
    }

    #endregion

    public static event Action<Character> DeathEvent;
    
    /// <summary>
    /// Utility function that returns the current direction axes being requested by the controller
    /// or the zero vector if no controller is assigned.
    /// </summary>
    protected Vector2 GetDirectionAxes() => (controller != null) 
                                          ? controller.GetAxes() 
                                          : Vector2.zero;
    /// <summary>
    /// Checks for input from the controller and handles its requests accordingly.
    /// </summary>
    private void DelegateControllerOrders()
    {
        if (Controller == null)
            return;

        Vector2 controllerAxes = GetDirectionAxes();
        if (controllerAxes.x != 0f)
        {
            Facing = controllerAxes.x;
        }

        if ((!Grounded && controller.IsRequestingBoost()))
        {
            StartFlying();
        }
        else
        {
            StopFlying();
        }

        if (controller.IsRequestingGrab())
        {
            if (currentlyGrabbedObject != null)
            {
                ThrowGrabbedObject();
            }
            else
            {
                TryGrabNearest();
            }
        }
    }

    private void WalkUpdate()
    {
        if (Grounded)
        {
            transform.position += Vector3.right * walkSpeed * Time.deltaTime * GetDirectionAxes().x;
        }
    }

    private void AnimatorUpdate()
    {
        animator?.SetBool("Grounded", Grounded);
        animator?.SetBool("Falling", Falling);

        if (GetDirectionAxes().x != 0 && !Boosting && Grounded)
        {
            animator?.SetBool("Walking", true);
        }
        else
        {
            animator?.SetBool("Walking", false);
        }
    }

    protected override void Destroy()
    {
        DeathEvent?.Invoke(this);

        Destroy(this.gameObject);
    }

    #region Unity Callbacks

    protected virtual void OnValidate()
    {
        CurrentFuel = currentFuel;
    }

    protected virtual void Awake()
    {
        groundLayerMask = 1 << LayerMask.NameToLayer("ground");

        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    protected virtual void Start()
    {
        var players = GameObject.FindGameObjectsWithTag("Player");
        foreach (var p in players)
        {
            var coll = p.GetComponent<Collider2D>();
            if (coll != null)
            {
                Physics2D.IgnoreCollision(coll, hitboxCollider, true);
            }
        }
    }

    protected virtual void FixedUpdate()
    {
        UpdateGroundedStatus();

        FlightFixedUpdate(GetDirectionAxes());
    }

    protected override void Update()
    {
        base.Update();

        DelegateControllerOrders();
        WalkUpdate();
        FuelUpdate();
        AnimatorUpdate();
        GrabUpdate();
    }

    #endregion Unity Callbacks
}