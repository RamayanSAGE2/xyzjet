﻿using System;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class JetpackFireEffect : MonoBehaviour
{
    private ParticleSystem particleSys;

    public void Play()
    {
        particleSys.Play();
    }

    public void Stop()
    {
        particleSys.Stop();
    }

    private void Awake()
    {
        particleSys = GetComponent<ParticleSystem>();

        if (particleSys.isPlaying)
            particleSys.Stop();
    }

}