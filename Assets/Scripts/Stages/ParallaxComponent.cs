﻿using UnityEngine;

public class ParallaxComponent : MonoBehaviour
{
    private Vector3 thisStartingPosition;
    private Vector3 targetStartingPosition;

    [SerializeField]
    private Transform target;
    public Transform Target
    {
        get => target;
        set  
        {
            target = value;

            if (value != null)
            {
                targetStartingPosition = target.transform.position;
            }
        }
    }

    [SerializeField, Range(0, 1f)]
    private float parallaxFactor = 0f;
    public float ParallaxFactor
    {
        get => parallaxFactor;
        set => parallaxFactor = Mathf.Clamp01(value);
    }

    private float originalZ;

    private void Start()
    {
        originalZ = transform.position.z;
        thisStartingPosition = transform.position;

        Target = Target;
    }

    private void Update()
    {
        if (Target == null)
            return;

        transform.position = thisStartingPosition + (target.transform.position - targetStartingPosition) * parallaxFactor;
        transform.position = new Vector3(transform.position.x, transform.position.y, originalZ);
    }
}