﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// SUBSTAGE IS A SUBPART OF A STAGE
/// A SUBSTAGE IS PLAYED INSIDE A STAGE AND ITS EVENTS MUST OCCOUR AS IT IS ACTIVATED
/// </summary>
public class SubStage : MonoBehaviour
{
    [Range(1,6)]
    public int substage_id;
    private Stage parent;

    [Header("Other info")]
    [SerializeField]
    private float transitionDelay = 5f;
    public float TransitionDelay => transitionDelay;

    [SerializeField]
    private Transform playerSpawnTransformGroup;

    [SerializeField]
    private Transform cameraTargetTransform;
    public Transform CameraTargetTransform => cameraTargetTransform;

    private void Awake(){
        this.parent = GetComponentInParent<Stage>();
    }

    public int GetStageParentID() {
        return this.parent.stage_id;
    }

    private void OnValidate()
    {
        if (cameraTargetTransform == null)
        {
            cameraTargetTransform = this.transform;
        }
    }

    public Vector3 GetPlayerSpawnPoint(int playerIdx)
    {
        if (playerSpawnTransformGroup.childCount > 0)
        {
            return playerSpawnTransformGroup.GetChild(playerIdx % playerSpawnTransformGroup.childCount).position;
        }
        else
        {
            return transform.position;
        }
    }
}

[CustomEditor(typeof(SubStage))]
public class SubStageEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();

        if (GUILayout.Button("Bring camera here"))
        {
            SubStage s = this.target as SubStage;

            float prevZ = Camera.main.transform.position.z;
            Camera.main.transform.position = s.CameraTargetTransform.position;
            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, prevZ);
        }
    }
}