﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Stage : MonoBehaviour
{
    public int stage_id;

    private SubStage[] substages;

    private List<SubStage> unplayedSubstages;
    
    public SubStage ActiveSubstage { get; private set; }

    public static Stage Current { get; private set; }

    private SubStage GetNextRandomSubstage()
    {
        int index = UnityEngine.Random.Range(0, unplayedSubstages.Count);
        SubStage ret = unplayedSubstages[index];

        // Only one is left, we must refill the unplayed substages list.
        if (unplayedSubstages.Count == 1)
        {
            foreach (var s in substages)
            {
                if (s != unplayedSubstages[0])
                {
                    unplayedSubstages.Add(s);
                }
            }
        }

        unplayedSubstages.RemoveAt(index % unplayedSubstages.Count);

        return ret;
    }

    /// <summary>
    /// Performs a smooth transition onto the next substage.
    /// </summary>
    /// <param name="onFinish">Action to be performed once the transition is finished.</param>
    public void TransitionToNextSubstage(Action onFinish = null, bool instant = true)
    {
        SubStage previousSubstage = ActiveSubstage;

        ActiveSubstage = GetNextRandomSubstage();

        if (!instant)
        {
            StartCoroutine(SubstageTransitionCoroutine(ActiveSubstage.TransitionDelay, onFinish, previousSubstage, ActiveSubstage));
        }
        else
        {
            WrapSubstageTransition(previousSubstage, ActiveSubstage);
            onFinish?.Invoke();
        }
    }

    private IEnumerator SubstageTransitionCoroutine(float totalTime, Action onFinish, SubStage previousSubstage, SubStage newSubStage)
    {
        float elapsed = 0f;
        Vector3 cameraOriginalPos = Camera.main.transform.position;
        Vector3 cameraDestinationPos = newSubStage.CameraTargetTransform.position;

        while (elapsed < totalTime)
        {
            elapsed += Time.unscaledDeltaTime;

            Camera.main.transform.position = Vector3.Lerp(
                cameraOriginalPos,
                newSubStage.transform.position, 
                TransitionInterpolationFunction(elapsed / totalTime));

            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, cameraOriginalPos.z);

            yield return new WaitForEndOfFrame();
        }

        WrapSubstageTransition(previousSubstage, newSubStage);

        onFinish?.Invoke();
    }

    private void WrapSubstageTransition(SubStage previousSubstage, SubStage newSubstage)
    {
        float prevZ = Camera.main.transform.position.z;
        Camera.main.transform.position = newSubstage.CameraTargetTransform.position;
        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, prevZ);
        newSubstage.gameObject.SetActive(true);
        
        foreach (var s in substages)
        {
            if (s != newSubstage)
            {
                s.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Function that determines the interpolation position of the camera during substage transition,
    /// with respect to a moment in time.
    /// </summary>
    /// <param name="t">
    /// The ratio T'/T, where T is the total time it takes to complete the transition and
    /// T' is the amount of seconds elapsed since the beginning of the transition.
    /// </param>
    /// <remarks>
    /// This function should always return 1 if t is 1.
    /// </remarks>
    private static float TransitionInterpolationFunction(float t) => t * t;
    
    private void Awake()
    {
        substages = GetComponentsInChildren<SubStage>(true);
        unplayedSubstages = new List<SubStage>(substages);

        // No substage is currently active, select one from the substage list.
        ActiveSubstage = GetNextRandomSubstage();

        WrapSubstageTransition(null, ActiveSubstage);
    }

    private void OnEnable()
    {
        Current = this;
    }

    private void OnDisable()
    {
        if (Current == this)
        {
            Current = null;
        }
    }

}