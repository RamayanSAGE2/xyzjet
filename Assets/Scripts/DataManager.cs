﻿using UnityEngine;
/// <summary>
/// GUARDA TODAS AS INFORMAÇÕES ESTÁTICAS QUE SÃO MUITO USADAS EM UM SÓ LUGAR
/// </summary>
/// 
public class DataManager : SingletonMonoBehaviour<DataManager> { 

    public Character[] Playable_Characters;
    private int id_next_playable_char = 0;

    public Projectile[] Projectiles;

    /// <summary>
    /// RETORNA O CHAR PELO NOME DELE, OS NOMES ATÉ AGORA SÃO 'Joe, Jonny, Hedgar e Guedes'
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public static Character GetPlayable_Character(string s) {
        Character playable_cha = null;
        foreach (Character cha in Instance.Playable_Characters) {
            if (cha.name.Equals(s)) playable_cha = cha;
        }
        return playable_cha;
    }
    
   /// <summary>
   /// RETORNA O PROXIMO CHAR
   /// </summary>
   /// <returns></returns>
    public static Character GetNextPlayable_Character(){
        if (Instance.Playable_Characters.Length > Instance.id_next_playable_char + 1)
        {
            Instance.id_next_playable_char++;
        }
        else {
            Instance.id_next_playable_char = 0;
        }
        if (Instance.Playable_Characters[Instance.id_next_playable_char] != null)
            return Instance.Playable_Characters[Instance.id_next_playable_char];
        else return null;
    }

    public static Projectile GetProjectile(int id) {
        if (id >= 0 && id <= Instance.Projectiles.Length)
            return Instance.Projectiles[id];
        else
            Debug.LogError("Invalid ID");
        return null;
    }
}