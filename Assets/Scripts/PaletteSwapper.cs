﻿// Source: https://github.com/Broxxar/PaletteSwapping/blob/master/Assets/Scripts/PaletteSwapMatrix.cs
using UnityEngine;

[ExecuteInEditMode]
public class PaletteSwapper : MonoBehaviour
{
    public Color Color0;
    public Color Color1;
    public Color Color2;
    public Color Color3;

    private Material material;

    private void OnEnable()
    {
        Shader shader = Shader.Find("Hidden/PaletteSwapMatrix");

        if (material == null)
        {
            material = new Material(shader);
        }
    }

    private void OnDisable()
    {
        if (material != null)
        {
            DestroyImmediate(material);
        }
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        material.SetMatrix("_ColorMatrix", ColorMatrix);
        Graphics.Blit(src, dst, material);
    }

    private Matrix4x4 ColorMatrix
    {
        get
        {
            Matrix4x4 mat = new Matrix4x4();
            mat.SetRow(0, ColorToVec(Color0));
            mat.SetRow(1, ColorToVec(Color1));
            mat.SetRow(2, ColorToVec(Color2));
            mat.SetRow(3, ColorToVec(Color3));

            return mat;
        }
    }

    private Vector4 ColorToVec(Color color)
    {
        return new Vector4(color.r, color.g, color.b, color.a);
    }
}