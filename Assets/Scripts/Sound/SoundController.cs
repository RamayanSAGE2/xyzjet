﻿using UnityEngine;

public enum SOUNDS_PLAYER{
    jump,
    land,
    jetpack_turn_on,
    jetpack_on,
    jetpack_turn_off,
    boost,
    dash,
    kick
}

public enum SOUNDS_MENU
{
    select,
    confirm,
    click,
}

public enum SOUNDS_ASTEROID
{
    impact
}

public class SoundController : MonoBehaviour {

    [Header("MENU SOUNDS")]
    public AudioClip sound_select;
    public AudioClip sound_confirm;
    public AudioClip sound_click;

    [Header("PLAYER SOUNDS")]
    public AudioClip sound_jump;
    public AudioClip sound_land;
    public AudioClip sound_jetpack_turn_on;
    public AudioClip sound_jetpack_on;
    public AudioClip sound_jetpack_turn_off;
    public AudioClip sound_boost;
    public AudioClip sound_dash;
    public AudioClip sound_kick;

    [Header("ASTEROID SOUNDS")]
    public AudioClip sound_impact;

    [Header("SYSTEM")]
    private static bool isSoundOn;
	public static SoundController instance;

    // Use this for initialization
    void Start () {
		instance = this;
		isSoundOn = true;
	}
	
	public static void PlaySoundEffect(SOUNDS_PLAYER currentSound){
        if (isSoundOn){
            switch (currentSound)
            {
                case SOUNDS_PLAYER.jump:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_jump);
                    break;
                case SOUNDS_PLAYER.land:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_land);
                    break;
                case SOUNDS_PLAYER.dash:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_dash);
                    break;
                case SOUNDS_PLAYER.jetpack_turn_on:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_jetpack_turn_on);
                    break;
                case SOUNDS_PLAYER.jetpack_on:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_jetpack_on);
                    break;
                case SOUNDS_PLAYER.jetpack_turn_off:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_jetpack_turn_off);
                    break;
                case SOUNDS_PLAYER.boost:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_boost);
                    break;
                case SOUNDS_PLAYER.kick:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_kick);
                    break;
            }
        }
	}

    public static void PlaySoundEffect(SOUNDS_MENU currentSound)
    {
        if (isSoundOn)
        {
            switch (currentSound)
            {
                case SOUNDS_MENU.click:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_click);
                    break;
                case SOUNDS_MENU.select:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_select);
                    break;
                case SOUNDS_MENU.confirm:
                    instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_confirm);
                    break;
            }
        }
    }

    public static void PlaySoundEffect(SOUNDS_ASTEROID currentSound)
    {
        if (isSoundOn)
        {
            switch (currentSound)
            {
                case SOUNDS_ASTEROID.impact:
                    //instance.GetComponent<AudioSource>().PlayOneShot(instance.sound_impact);
                    break;
            }
        }
    }



    public void ToggleSound(){
        isSoundOn = !isSoundOn;
        SetSound(isSoundOn);
    }

    public void SetSound(bool b) {
        isSoundOn = b;
    }

	public static bool isSoundOnNow(){
		return isSoundOn;
	}

}
