﻿using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class Grabbable : MonoBehaviour
{
    public Rigidbody2D Rigidbody { get; private set; }
    public Collider2D Collider { get; private set; }

    public GameObject Grabber { get; private set; } = null;

    /// <summary>
    /// Invoked when this object is grabbed.
    /// </summary>
    public event Action<GameObject> GrabEvent;

    /// <summary>
    /// Invoked when this object is thrown.
    /// </summary>
    public event Action<GameObject> ThrowEvent;

    public void OnGrab(GameObject grabber)
    {
        Grabber = grabber;
        var destroyable = GetComponent<Destroyable>();
        if (destroyable != null)
        {
            destroyable.Invulnerable = true;
        }

        GrabEvent?.Invoke(grabber);
    }

    public void OnThrow()
    {
        var go = Grabber;
        Grabber = null;
        var destroyable = GetComponent<Destroyable>();
        if (destroyable != null)
        {
            destroyable.Invulnerable = false;
        }

        ThrowEvent?.Invoke(go);
    }

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
        Collider = GetComponent<Collider2D>();
    }
}