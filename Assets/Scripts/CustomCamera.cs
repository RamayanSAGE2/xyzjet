﻿using UnityEngine;

public class CustomCamera : MonoBehaviour
{
    [SerializeField]
    private Transform target;

    [SerializeField]
    private bool followTarget = false;

    [SerializeField]
    [Range(0, 1)]
    private float smoothFactor = 0.1f;

    private void LateUpdate()
    {
        if (followTarget && target != null)
        {
            float newX = Mathf.Lerp(transform.position.x, target.position.x, (1 / smoothFactor) * Time.deltaTime);
            float newY = Mathf.Lerp(transform.position.y, target.position.y, (1 / smoothFactor) * Time.deltaTime);
            transform.position = new Vector3(newX, newY, transform.position.z);
        }
    }
}